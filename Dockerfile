FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

RUN apt update && apt install -y git make clang gcc libsdl2-dev
RUN git clone https://gitlab.com/lsde-stuff/shoebill.git /shoebill

WORKDIR /shoebill
RUN make make_core
WORKDIR /shoebill/sdl-gui
RUN ./lin_build.sh

FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base
LABEL "EAAS_EMULATOR_TYPE"="shoebill"
LABEL "EAAS_EMULATOR_VERSION"="0.0.5"

RUN apt update && apt install -y libsdl2-2.0-0 && rm -rf /var/cache/apt/*
COPY --from=0 /shoebill/sdl-gui/shoebill /usr/local/bin
ADD metadata /metadata

